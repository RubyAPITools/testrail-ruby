require 'spec_helper'
require_relative '../../lib/testrail-ruby'

# Must set these to run tests
# gitlab varables, .bashrc, etc

base_url = "https://test/"
user     = "username"
password = "password"

client = TestRail::APIClient.new(base_url)
client.user = user
client.password = password

#### CASES ##############+++++5
  RSpec.describe "get_case" do
    it 'request is formatted correctly' do
      stub = stub_request(:get, "https://test/index.php?/api/v2/get_case/1462").
                          with(headers: {
                            'Accept'=>'*/*',
                            'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                            'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                            'Content-Type'=>'application/json',
                            'User-Agent'=>'Ruby'}).
                          to_return(status: 200, body: "", headers: {})

      client.get_case(1462)
      expect(stub).to have_been_requested
    end
  end #describe

  RSpec.describe "get_cases" do
    it 'request is formatted correctly' do
      stub =  stub_request(:get, "https://test/index.php?/api/v2/get_cases/1&section_id=1&suite_id=1").
                          with(headers: {
                            'Accept'=>'*/*',
                            'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                            'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                            'Content-Type'=>'application/json',
                            'User-Agent'=>'Ruby'}).
                          to_return(status: 200, body: "", headers: {})

      client.get_cases(1, {"suite_id":1, "section_id":1})
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "add_case" do
    it 'request is formatted correctly' do
      stub =  stub_request(:post, "https://test/index.php?/api/v2/add_case/1").
                          with(body: "{\"title\":\"testCaseName\",\"type_id\":1}",
                          headers: {
                            'Accept'=>'*/*',
                            'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                            'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                            'Content-Type'=>'application/json',
                            'User-Agent'=>'Ruby'}).
                          to_return(status: 200, body: "", headers: {})

      client.add_case(1, {"title": "testCaseName", "type_id": 1})
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "update_case" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/update_case/1").
               with(body: "{\"title\":\"testCaseName\",\"type_id\":1}",
                    headers: {
                      'Accept'=>'*/*',
                      'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                      'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                      'Content-Type'=>'application/json',
                      'User-Agent'=>'Ruby'}).
               to_return(status: 200, body: "", headers: {})

      client.update_case(1, {"title": "testCaseName", "type_id": 1})
      expect(stub).to have_been_requested
    end
  end #describe

  RSpec.describe "delete_case" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/delete_case/1").
               with(body: "{\"title\":\"testCaseName\",\"type_id\":1}",
                    headers: {
                      'Accept'=>'*/*',
                      'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                      'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                      'Content-Type'=>'application/json',
                      'User-Agent'=>'Ruby'}).
               to_return(status: 200, body: "", headers: {})

      client.delete_case(1, {"title": "testCaseName", "type_id": 1})
      expect(stub).to have_been_requested
    end
  end #describe

#### SUITES #############+++++5
  RSpec.describe "get_suite" do
    it 'is formatted correctly' do
      stub = stub_request(:get, "https://test/index.php?/api/v2/get_suite/1").
                          with(headers: {
                            'Accept'=>'*/*',
                            'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                            'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                            'Content-Type'=>'application/json',
                            'User-Agent'=>'Ruby'}).
                          to_return(status: 200, body: "", headers: {})

      client.get_suite(1)
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "get_suites" do
    it 'is formatted correctly' do
      stub = stub_request(:get, "https://test/index.php?/api/v2/get_suites/1").
                          with(headers: {
                            'Accept'=>'*/*',
                            'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                            'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                            'Content-Type'=>'application/json',
                            'User-Agent'=>'Ruby'}).
                          to_return(status: 200, body: "", headers: {})
      client.get_suites(1)
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "add_suite" do
    it 'request is formatted correctly' do
      stub =  stub_request(:post, "https://test/index.php?/api/v2/add_suite/1").
              with(body: "{\"name\":\"suite name\",\"description\":\"description of test suite\"}",
                headers: {
                  'Accept'=>'*/*',
                  'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                  'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                  'Content-Type'=>'application/json',
                  'User-Agent'=>'Ruby'}).
              to_return(status: 200, body: "", headers: {})

      client.add_suite(1, {"name": "suite name", "description": "description of test suite"})
      expect(stub).to have_been_requested
    end
  end #describe

  RSpec.describe "update_suite" do
    it 'request is formatted correctly' do
      stub =  stub_request(:post, "https://test/index.php?/api/v2/update_suite/1").
              with(body: "{\"name\":\"suite name\",\"description\":\"description of test suite\"}",
                headers: {
                  'Accept'=>'*/*',
                  'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                  'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                  'Content-Type'=>'application/json',
                  'User-Agent'=>'Ruby'}).
              to_return(status: 200, body: "", headers: {})

      client.update_suite(1, {"name": "suite name", "description": "description of test suite"})
      expect(stub).to have_been_requested
    end
  end #describe

  RSpec.describe "delete_suite" do
    it 'request is formatted correctly' do
      stub =  stub_request(:post, "https://test/index.php?/api/v2/delete_suite/1").
              with(body: "{}",
                headers: {
                  'Accept'=>'*/*',
                  'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                  'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                  'Content-Type'=>'application/json',
                  'User-Agent'=>'Ruby'}).
              to_return(status: 200, body: "", headers: {})

      client.delete_suite(1)
      expect(stub).to have_been_requested
    end
  end #describe

#### SECTIONS ###########+++++5
  RSpec.describe "get_section" do
    it 'request is formatted correctly' do
      stub = stub_request(:get, "https://test/index.php?/api/v2/get_section/1").
                          with(headers: {
                            'Accept'=>'*/*',
                            'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                            'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                            'Content-Type'=>'application/json',
                            'User-Agent'=>'Ruby'}).
                          to_return(status: 200, body: "", headers: {})

      client.get_section(1)

      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "get_sections" do
    it 'is formatted correctly' do
      stub = stub_request(:get, "https://test/index.php?/api/v2/get_sections/1&section_id=1&suite_id=1").
                          with(headers: {
                            'Accept'=>'*/*',
                            'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                            'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                            'Content-Type'=>'application/json',
                            'User-Agent'=>'Ruby'}).
                          to_return(status: 200, body: "", headers: {})
      client.get_sections(1, {
        "suite_id":1,
        "section_id":1
        })

      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "add_section" do
    it 'is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/add_section/1").
         with(body: "{\"suite_id\":1,\"name\":\"SectionName\",\"description\":\"sectionDesc\",\"parent_id\":1,\"section_id\":1}",
              headers: {
                'Accept'=>'*/*',
                'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                'Content-Type'=>'application/json',
                'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.add_section(1, {
        "suite_id":1,
        "name": "SectionName",
        "description": "sectionDesc",
        "parent_id": 1,
        "section_id":1
        })

      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "update_section" do
    it 'is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/update_section/1").
         with(body: "{\"name\":\"SectionName\",\"description\":\"sectionDesc\"}",
              headers: {
                'Accept'=>'*/*',
                'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                'Content-Type'=>'application/json',
                'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.update_section(1, {
        "name": "SectionName",
        "description": "sectionDesc",
        })

      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "delete_section" do
    it 'is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/delete_section/1").
         with(body: "{}",
              headers: {
                'Accept'=>'*/*',
                'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                'Content-Type'=>'application/json',
                'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.delete_section(1)

      expect(stub).to have_been_requested
    end # it block
  end

#### MILESTONES #########+++++5
  RSpec.describe "get_milestone" do
    it 'request is formatted correctly' do
      stub = stub_request(:get, "https://test/index.php?/api/v2/get_milestone/1").
                          with(headers: {
                            'Accept'=>'*/*',
                            'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                            'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                            'Content-Type'=>'application/json',
                            'User-Agent'=>'Ruby'}).
                          to_return(status: 200, body: "", headers: {})

      client.get_milestone(1)

      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "get_milestones" do
    it 'request is formatted correctly' do
      stub = stub_request(:get, "https://test/index.php?/api/v2/get_milestones/1&is_completed=0&is_started=1").
                          with(headers: {
                            'Accept'=>'*/*',
                            'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                            'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                            'Content-Type'=>'application/json',
                            'User-Agent'=>'Ruby'}).
                          to_return(status: 200, body: "", headers: {})

      client.get_milestones(1, {"is_completed": 0, "is_started": 1})

      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "add_milestone" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/add_milestone/1").
              with(
                body: "{\"name\":\"Release 2.0\",\"due_on\":1394596385}",
                headers: {
                'Accept'=>'*/*',
                'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                'Content-Type'=>'application/json',
                'User-Agent'=>'Ruby'}).
              to_return(status: 200, body: "", headers: {})

      client.add_milestone(1, {"name": "Release 2.0", "due_on": 1394596385})

      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "update_milestone" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/update_milestone/1").
              with(body: "{\"is_completed\":false,\"is_started\":false,\"parent_id\":1,\"start_on\":1394596385}",
              headers: {
                'Accept'=>'*/*',
                'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                'Content-Type'=>'application/json',
                'User-Agent'=>'Ruby'}).
              to_return(status: 200, body: "", headers: {})

      client.update_milestone(1, {"is_completed": false, "is_started": false, "parent_id": 1, "start_on": 1394596385})

      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "delete_milestone" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/delete_milestone/1").
         with(body: "{}",
              headers: {
                'Accept'=>'*/*',
                'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                'Content-Type'=>'application/json',
                'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.delete_milestone(1)

      expect(stub).to have_been_requested
    end
  end

#### PLANS ##########+++++++++9
  RSpec.describe "get_plan" do
    it 'is formatted correctly' do
        stub = stub_request(:get, "https://test/index.php?/api/v2/get_plan/21").
                            with(headers: {
                              'Accept'=>'*/*',
                              'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                              'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                              'Content-Type'=>'application/json',
                              'User-Agent'=>'Ruby'}).
                            to_return(status: 200, body: "", headers: {})
        client.get_plan(21)
        expect(stub).to have_been_requested
    end
  end

  RSpec.describe "get_plans" do
    it 'request is formatted correctly' do
      stub = stub_request(:get, "https://test/index.php?/api/v2/get_plans/1&is_completed=1&milestone_id=1").
         with(
          headers: {'Accept'=>'*/*',
            'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
            'Content-Type'=>'application/json',
            'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.get_plans(1, {"is_completed":1, "milestone_id": 1 })
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "add_plan" do
    it 'request is formatted correctly' do
      stub =  stub_request(:post, "https://test/index.php?/api/v2/add_plan/1").
              with(
                body:
                "{\"name\":\"System test\",\"entries\":[{\"suite_id\":1,\"name\":\"Custom run name\",\"assignedto_id\":1},{\"suite_id\":1,\"include_all\":false,\"case_ids\":[1,2,3,5]}]}",
                headers: {
                  'Accept'=>'*/*',
                  'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                  'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                  'Content-Type'=>'application/json',
                  'User-Agent'=>'Ruby'}).
                to_return(status: 200, body: "", headers: {})

      options = {
                  "name": "System test",
                  "entries": [
                    {
                      "suite_id": 1,
                      "name": "Custom run name",
                      "assignedto_id": 1
                    },
                    {
                      "suite_id": 1,
                      "include_all": false,
                      "case_ids": [1,2,3,5]
                    }
                  ]
                }
      client.add_plan(1, options)

      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "add_plan_entry" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/add_plan_entry/1").
              with(
                body: "{\"suite_id\":1,\"assignedto_id\":1,\"include_all\":true,\"config_ids\":[1,2,4,5,6],\"runs\":[{\"include_all\":false,\"case_ids\":[1,2,3],\"config_ids\":[2,5]},{\"include_all\":false,\"case_ids\":[1,2,3,5,8],\"assignedto_id\":2,\"config_ids\":[2,6]}]}",
                headers: {'Accept'=>'*/*',
                  'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                  'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                  'Content-Type'=>'application/json',
                  'User-Agent'=>'Ruby'}).
                to_return(status: 200, body: "", headers: {})

      options =
       {
         "suite_id": 1,
         "assignedto_id": 1,
         "include_all": true,
         "config_ids": [1, 2, 4, 5, 6],
         "runs": [
           {
             "include_all": false,
             "case_ids": [1, 2, 3],
             "config_ids": [2, 5]
           },
           {
             "include_all": false,
             "case_ids": [1, 2, 3, 5, 8],
             "assignedto_id": 2,
             "config_ids": [2, 6]
           }
         ]
       }
      client.add_plan_entry(1, options)
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "update_plan" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/update_plan/1").
             with(
              body: "{\"name\":\"foo\",\"description\":\"bar\"}",
              headers: {'Accept'=>'*/*',
                'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                'Content-Type'=>'application/json',
                'User-Agent'=>'Ruby'}).
              to_return(status: 200, body: "", headers: {})

      client.update_plan(1, {"name": "foo", "description": "bar"})

      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "update_plan_entry" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/update_plan_entry/1/1").
              with(
                body: "{\"name\":\"foo\",\"description\":\"bar\"}",
                headers: {'Accept'=>'*/*',
                  'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                  'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                  'Content-Type'=>'application/json',
                  'User-Agent'=>'Ruby'}).
              to_return(status: 200, body: "", headers: {})

      client.update_plan_entry(1, 1, {"name": "foo", "description": "bar"})
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "close_plan" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/close_plan/1").
         with(body: "{}",
              headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.close_plan(1)
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "delete_plan" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/delete_plan/1").
         with(body: "{}",
              headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.delete_plan(1)
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "delete_plan_entry" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/delete_plan_entry/1/1").
         with(body: "{}",
              headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.delete_plan_entry(1, 1)
      expect(stub).to have_been_requested
    end
  end

#### PROJECTS ###########+++++5
  RSpec.describe "get_project" do
    it 'is formatted correctly' do
        stub = stub_request(:get, "https://test/index.php?/api/v2/get_project/1").
                            with(headers: {
                              'Accept'=>'*/*',
                              'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                              'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                              'Content-Type'=>'application/json',
                              'User-Agent'=>'Ruby'}).
                            to_return(status: 200, body: "", headers: {})
        client.get_project(1)
        expect(stub).to have_been_requested
    end
  end

  RSpec.describe "get_projects" do
    it 'is passes options correctly ' do
        stub = stub_request(:get, "https://test/index.php?/api/v2/get_projects&is_completed=0").
                            with(headers: {
                              'Accept'=>'*/*',
                              'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                              'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
                              'Content-Type'=>'application/json',
                              'User-Agent'=>'Ruby'}).
                            to_return(status: 200, body: "", headers: {})

        client.get_projects({"is_completed":0})
        expect(stub).to have_been_requested
    end
  end

  RSpec.describe "add_project" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/add_project").
         with(body: "{\"name\":\"foo\",\"announcement\":\"bar\",\"show_announcement\":true,\"suite_mode\":1}",
              headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.add_project({"name": "foo", "announcement": "bar", "show_announcement": true, "suite_mode": 1})
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "update_project" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/update_project/1").
         with(body: "{\"name\":\"foo\",\"announcement\":\"bar\",\"show_announcement\":true,\"suite_mode\":1,\"is_completed\":true}",
              headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.update_project(1, {"name": "foo", "announcement": "bar", "show_announcement": true, "suite_mode": 1, "is_completed": true})
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "delete_project" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/delete_project/1").
         with(body: "{}",
              headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})


      client.delete_project(1)
      expect(stub).to have_been_requested
    end
  end

#### RESULTS ##########+++++++7
  RSpec.describe "get_results" do
    it 'request is formatted correctly' do
      stub = stub_request(:get, "https://test/index.php?/api/v2/get_results/1&limit=10&status_id=4").
         with(headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.get_results(1, {"status_id": 4, "limit": 10})
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "get_results_for_case" do
    it 'request is formatted correctly' do
      stub = stub_request(:get, "https://test/index.php?/api/v2/get_results_for_case/1/2&limit=10&status_id=4").
         with(headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.get_results_for_case(1, 2, {"status_id": 4, "limit": 10})
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "get_results_for_run" do
    it 'request is formatted correctly' do
      stub = stub_request(:get, "https://test/index.php?/api/v2/get_results_for_run/1&created_after=12345&created_before=12345&created_by=1&limit=5&status_id=1").
         with(headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.get_results_for_run(1, {"created_after": 12345, "created_before": 12345, "created_by": 1, "limit": 5, "status_id": 1})
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "add_result" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/add_result/1").
         with(body: "{\"status_id\":5}",
              headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.add_result(1, {"status_id": 5})
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "add_result_for_case" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/add_result_for_case/1/1").
         with(body: "{\"status_id\":5}",
              headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.add_result_for_case(1, 1, {"status_id": 5})
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "add_results" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/add_results/1").
         with(body: "{\"foo\":\"bar\"}",
              headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.add_results(1, {"foo": "bar"})
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "add_results_for_cases" do
    it 'request is formatted correctly' do
      stub =  stub_request(:post, "https://test/index.php?/api/v2/add_results_for_cases/1").
         with(body: "{\"foo\":\"bar\"}",
              headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.add_results_for_cases(1, {"foo": "bar"})
      expect(stub).to have_been_requested
    end
  end

#### RUNS ##############++++++6

  RSpec.describe "get_run" do
    it 'request is formatted correctly' do
      stub = stub_request(:get, "https://test/index.php?/api/v2/get_run/1").
         with(headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.get_run(1)
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "get_runs" do
    it 'request is formatted correctly' do
      stub = stub_request(:get, "https://test/index.php?/api/v2/get_runs/1&biz=baz&foo=bar").
         with(headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.get_runs(1, {"foo": "bar", "biz": "baz"})
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "add_run" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/add_run/1").
         with(body: "{\"foo\":\"bar\",\"biz\":\"baz\"}",
              headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.add_run(1, {"foo": "bar", "biz": "baz"})
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "update_run" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/update_run/1").
         with(body: "{\"foo\":\"bar\",\"biz\":\"baz\"}",
              headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.update_run(1, {"foo": "bar", "biz": "baz"})
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "close_run" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/close_run/1").
         with(body: "{}",
              headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.close_run(1)
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "delete_run" do
    it 'request is formatted correctly' do
      stub = stub_request(:post, "https://test/index.php?/api/v2/delete_run/1").
         with(body: "{}",
              headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.delete_run(1)
      expect(stub).to have_been_requested
    end
  end

#### TESTS #################--2

  RSpec.describe "get_test" do
    it 'request is formatted correctly' do
      stub = stub_request(:get, "https://test/index.php?/api/v2/get_test/1").
         with(headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.get_test(1)
      expect(stub).to have_been_requested
    end
  end

  RSpec.describe "get_tests" do
    it 'request is formatted correctly' do
      stub = stub_request(:get, "https://test/index.php?/api/v2/get_tests/1&status_id=1").
         with(headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

      client.get_tests(1, {"status_id": 1})
      expect(stub).to have_been_requested
    end
  end
