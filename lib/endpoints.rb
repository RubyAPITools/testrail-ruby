module Endpoints

#### CASES ###############+++++5

  ####get_case###############+
    # Returns an existing test case
    # @param case_id [int] The id of the test case you want
    # @example Code
    #   @client.get_case(1)
    # @example Endpoint
    #   GET index.php?/api/v2/get_case/1
    # @example Response
    #    {
    #      "created_by": 5,s
    #      "created_on": 1392300984,
    #      "custom_expected": "..",
    #      "custom_preconds": "..",
    #      "custom_steps": "..",
    #      "custom_steps_separated": [
    #          {
    #              "content": "Step 1",
    #              "expected": "Expected Result 1"
    #          },
    #          {
    #              "content": "Step 2",
    #              "expected": "Expected Result 2"
    #         }
    #      ],
    #      "estimate": "1m 5s",
    #      "estimate_forecast": null,
    #      "id": 1,
    #      "milestone_id": 7,
    #      "priority_id": 2,
    #      "refs": "RF-1, RF-2",
    #      "section_id": 1,
    #      "suite_id": 1,
    #      "title": "Change document attributes (author, title, organization)",
    #      "type_id": 4,
    #      "updated_by": 1,
    #      "updated_on": 1393586511
    #    }
    # @see http://docs.gurock.com/testrail-api2/reference-cases
    def get_case(case_id)
      send_get("get_case/#{case_id}")
    end

  ####get_cases##############+
      # Returns a list of test cases for a test suite or specific section in a test suite.
      # @param project_id [int] The id of the project that contains your tests
      # @param [Hash] opts
      # @option opts [int]            :suite_id       The ID of the test suite (optional if the project is operating in single suite mode)
      # @option opts [int]            :section_id     The ID of the section (optional)
      # @option opts [unix timestamp] :created_after  Only return test cases created after this date (as UNIX timestamp).
      # @option opts [unix timestamp] :created_before Only return test cases created before this date (as UNIX timestamp).
      # @option opts [int(list)]      :created_by     A comma-separated list of creators (user IDs) to filter by.
      # @option opts [int(list)]      :milestone_id   A comma-separated list of milestone IDs to filter by (not available if the milestone field is disabled for the project).
      # @option opts [int(list)]      :priority_id    A comma-separated list of priority IDs to filter by.
      # @option opts [int(list)]      :template_id    A comma-separated list of template IDs to filter by (requires TestRail 5.2 or later)
      # @option opts [int(list)]      :type_id        A comma-separated list of case type IDs to filter by.
      # @option opts [unix timestamp] :updated_after  Only return test cases updated after this date (as UNIX timestamp).
      # @option opts [unix timestamp] :updated_before Only return test cases updated before this date (as UNIX timestamp).
      # @option opts [int(list)]      :updated_by     A comma-separated list of users who updated test cases to filter by.
      # @example Code Example
      #   @client.get_cases(1, {"suite_id":1, "section_id":1})
      # @example Endpoint Example
      #   GET index.php?/api/v2/get_cases/1&suite_id=1&section_id=1
      # @example Response Example
      #   [
      #    { "id": 1, "title": "..", .. },
      #    { "id": 2, "title": "..", .. },
      #    ..
      #   ]
      # @see http://docs.gurock.com/testrail-api2/reference-cases
      def get_cases(project_id, opts = {})
        options = param_stringify(opts)
        send_get("get_cases/#{project_id.to_s}&#{options}")
      end

  ####add_case###############+
    # Creates a new test case
    # @param section_id [int] The ID of the section the test case should be added to
    # @param [Hash] opts
    # @option opts [string]   :title          The title of the test case (required)
    # @option opts [int]      :template_id    The ID of the template (field layout) (requires TestRail 5.2 or later)
    # @option opts [int]      :type_id        The ID of the case type
    # @option opts [int]      :priority_id    The ID of the case priority
    # @option opts [timespan] :estimate       The estimate, e.g. "30s" or "1m 45s"
    # @option opts [int]      :milestone_id   The ID of the milestone to link to the test case
    # @option opts [string]   :refs           A comma-separated list of references/requirements
    # @option opts [varies]   :custom_fields  Custom fields are supported as well and must be submitted with their system name, prefixed with 'custom_'
    # @example Code Example
    #   @client.add_case(1, {"title":"testCaseName", "type_id":1})
    # @example Endpoint Example
    #   index.php?/api/v2/add_case/1&title="foo"&type_id=1
    # @see http://docs.gurock.com/testrail-api2/reference-cases
    # @note For more information about custom fields, see guroc docs
    def add_case(section_id, opts = {})
      send_post("add_case/#{section_id.to_s}", opts)
    end

  ####update_case############+
    # Update test result by case id
    # @param case_id [int] The id of the test case
    # @param [Hash] opts
    # @option opts [string]   :title          The title of the test case (required)
    # @option opts [int]      :template_id    The ID of the template (field layout) (requires TestRail 5.2 or later)
    # @option opts [int]      :type_id        The ID of the case type
    # @option opts [int]      :priority_id    The ID of the case priority
    # @option opts [timespan] :estimate       The estimate, e.g. "30s" or "1m 45s"
    # @option opts [int]      :milestone_id   The ID of the milestone to link to the test case
    # @option opts [string]   :refs           A comma-separated list of references/requirements
    # @option opts [varies]   :custom_fields  Custom fields are supported as well and must be submitted with their system name, prefixed with 'custom_'
    # @example Code
    #   client.update_case(1, {"title": "testCaseName", "type_id": 1})
    # @example Endpoint
    #   index.php?/api/v2/update_case/1
    # @example Body
    #   {\"title\":\"testCaseName\",\"type_id\":1}
    # @see http://docs.gurock.com/testrail-api2/reference-cases
    def update_case(case_id, opts = {})
      send_post("update_case/#{case_id.to_s}", opts)
    end

  ####delete_case############+
    # Delete test case by case id
    # @param case_id [int] The id of the test case
    # @param [Hash] opts
    # @example Code Example
    #   client.delete_case(1, {"title": "testCaseName", "type_id": 1})
    # @example Endpoint Example
    #   index.php?/api/v2/delete_case/1
    # @example Body Example
    #   {\"title\":\"testCaseName\",\"type_id\":1}
    # @see http://docs.gurock.com/testrail-api2/reference-cases
    def delete_case(case_id, opts = {})
      send_post("delete_case/#{case_id.to_s}", opts)
    end

#### SUITES ##############+++++5

  ####get_suite##############+
    # Return suite by suite id
    # @param suite_id [int] The suite id
    # @param [Hash] opts
    # @example Code
    #   client.get_suit(1)
    # @example Endpoint
    #   index.php?/api/v2/get_suite/1
    # @example Response
    #   {
    #     "description": "..",
    #     "id": 1,
    #     "name": "Setup & Installation",
    #     "project_id": 1,
    #     "url": "http://<server>/testrail/index.php?/suites/view/1"
    #   }
    # @see http://docs.gurock.com/testrail-api2/reference-suites
    def get_suite(suite_id)
      send_get("get_suite/#{suite_id.to_s}")
    end

  ####get_suites#############+
    # Return all suites in project by project id
    # @param project_id (see get_cases)
    # @param [Hash] opts
    # @example Code
    #   client.get_suites(1)
    # @example Endpoint
    #   index.php?/api/v2/get_suites/:project_id
    # @example Response
    #   [
    #     { "id": 1, "name": "Setup & Installation", .. },
    #     { "id": 2, "name": "Document Editing", .. }
    #   ]
    # @see http://docs.gurock.com/testrail-api2/reference-suites
    def get_suites(project_id)
      send_get("get_suites/#{project_id.to_s}")
    end

  ####add_suite##############+
    # Add a test suite
    # @param project_id [int] The id of the project containing suites
    # @param [Hash] opts
    # @option opts [string]   :name         Name of new suite
    # @option opts [string]   :description  Description of new suite
    # @example Code
    #   client.add_suite(1, {"name": "suite name", "description": "description text"})
    # @example Endpoint
    #   index.php?/api/v2/add_suite/1
    # @example Request Body
    #   {"name":"suite name","description": "description of test suite"}
    # @example Response
    #   { "name": "suite name",
    #     "description": "description text"
    #   }
    # @see http://docs.gurock.com/testrail-api2/reference-suites
    def add_suite(project_id, opts = {})
      send_post("add_suite/#{project_id.to_s}", opts)
    end

  ####update_suite###########+
    # update a test suite
    # @param suite_id [int] The suite id
    # @param [Hash] opts
    # @option opts [string]   :name         Name of new suite
    # @option opts [string]   :description  Description of new suite
    # @example Endpoint Example
    #   index.php?/api/v2/update_suite/:suite_id
    # @see http://docs.gurock.com/testrail-api2/reference-suites
    def update_suite(suite_id, opts = {})
      send_post("update_suite/#{suite_id.to_s}", opts)
    end

  ####delete_suite###########+
    # Delete a test suite
    # @param suite_id [int] The suite id
    # @example Code
    #   client.delete_suite(1)
    # @example Endpoint
    #   index.php?/api/v2/delete_suite/1
    # @see http://docs.gurock.com/testrail-api2/reference-suites
    def delete_suite(suite_id, opts = {})
      send_post("delete_suite/#{suite_id.to_s}", opts)
    end

#### SECTIONS ############+++++5

  ####get_section#############
    # Return section by id
    # @param [int]  section_id
    # @example Endpoint
    #   GET index.php?/api/v2/get_section/1
    # @example Code
    #   client.get_section(2)
    # @example Return
    #   {
    #     "depth": 0,
    #     "description": null,
    #     "display_order": 1,
    #     "id": 1,
    #     "name": "section name",
    #     "parent_id": 1,
    #     "suite_id": 2
    #   }
    # @see http://docs.gurock.com/testrail-api2/reference-sections
    def get_section(section_id)
      send_get("get_section/#{section_id.to_s}")
    end

  ####get_sections############
    # Get sections for suite
    # @param [int]  project_id
    # @param [Hash] opts
    # @options opts [int] :suite_id
    # @example Endpoint
    #   GET index.php?/api/v2/get_sections/1&suite_id=2
    # @example Code
    #   client.get_sections(1, {"suite_id":2})
    # @example Return
    #    [
    #      {
    #        "depth": 0,
    #        "display_order": 1,
    #        "id": 1,
    #        "name": "Prerequisites",
    #        "parent_id": null,
    #        "suite_id": 1
    #      },
    #      {
    #        "depth": 0,
    #        "display_order": 2,
    #        "id": 2,
    #        "name": "Documentation & Help",
    #        "parent_id": null,
    #        "suite_id": 1
    #      },
    #      {
    #        "depth": 1, // A child section
    #        "display_order": 3,
    #        "id": 3,
    #        "name": "Licensing & Terms",
    #        "parent_id": 2, // Points to the parent section
    #        "suite_id": 1
    #      },
    #      ..
    #    ]
    # @see http://docs.gurock.com/testrail-api2/reference-sections
    def get_sections(project_id, opts = {})
      options = param_stringify(opts)
      send_get("get_sections/#{project_id.to_s}&#{options}")
    end

  ####add_section#############
    # Add section to suite
    # @param [int]  project_id
    # @param [Hash] opts
    # @option opts [Int]    suite_id
    # @option opts [String] name
    # @option opts [Int]    parent_id
    # @example Endpoint
    #   POST index.php?/api/v2/add_section/1
    # @example Code
    #   @client.add_section(1, {"suite_id": 2, "name": "Name of new section.", "parent_id": 1})
    # @example Request
    #   {
    #     "name": "Name of new section.",
    #     "suite_id": 2,
    #     "parent_id": 1
    #   }
    # @example Return
    #   If successful, this method returns the updated section using the same response format as get_section.
    # @see http://docs.gurock.com/testrail-api2/reference-sections
    def add_section(project_id, opts = {})
      send_post("add_section/#{project_id.to_s}", opts)
    end

  ####update_section##########
    # Update a section
    # @param [Int]  section_id
    # @param [Hash] opts
    # @option opts [string] description
    # @option opts [string] name
    # @example Endpoint
    #   POST index.php?/api/v2/update_section/1
    # @example Code
    #   index.php?/api/v2/update_section/1
    # @example Request
    #   {
    #     "name": "updated section name",
    #     "description": "updated description"
    #   }
    # @example Return
    #   If successful, this method returns the updated section using the same response format as get_section.
    # @see http://docs.gurock.com/testrail-api2/reference-sections
    def update_section(section_id, opts = {})
      send_post("update_section/#{section_id.to_s}", opts)
    end

  ####delete_section###########
    # Delete a section WARNING: Destructive
    # @param [Int]  section_id
    # @example Endpoint
    #   POST index.php?/api/v2/delete_section/1
    # @example Code
    #   client.delete_section(1)
    # @see http://docs.gurock.com/testrail-api2/reference-sections
    def delete_section(section_id, opts = {})
      send_post("delete_section/#{section_id}", opts)
    end

#### MILESTONES ##########+++++5

  ####get_milestone############
    # Get milestone by milestone_id
    # @param [Int]  milestone_id
    # @example Endpoint
    #   GET index.php?/api/v2/get_milestone/1
    # @example Code
    #   client.get_milestone(1)
    # @example Return
    #   {
    #      "completed_on": 1389968184,
    #      "description": "...",
    #      "due_on": 1391968184,
    #      "id": 1,
    #      "is_completed": true,
    #      "name": "Release 1.5",
    #      "project_id": 1,
    #      "url": "http://<server>/testrail/index.php?/milestones/view/1"
    #   }
    # @see http://docs.gurock.com/testrail-api2/reference-milestones
    def get_milestone(milestone_id)
      send_get("get_milestone/#{milestone_id.to_s}")
    end

  ####get_milestones##########
    # Get project milestones by project id
    # @param [int]  project_id
    # @param [Hash] opts (optional)
    # @options opts [bool] is_started
    # @options opts [bool] is_completed
    # @example Endpoint
    #   index.php?/api/v2/get_milestones/1&is_completed=0&is_started=1
    # @example Code
    #   client.get_milestones(1, {"is_completed": 0, "is_started": 1})
    # @example Return
    #   [{ "id": 1, "name": "foo", .. }, { "id": 1, "name": "bar, .. }]
    # @see http://docs.gurock.com/testrail-api2/reference-milestones
    def get_milestones(project_id, opts = {})
      options = param_stringify(opts)
      send_get("get_milestones/#{project_id.to_s}&#{options}")
    end

  ####add_milestone###########
    # Add milestone to project id
    # @param project_id [int]
    # @param [Hash] opts
    # @option opts [string]     name
    # @option opts [string]     description
    # @option opts [timestamp]  due_on
    # @option opts [int]        parent_id
    # @option opts [timestamp]  start_on
    # @example Endpoint
    #   index.php?/api/v2/add_milestone/1
    # @example Code
    #   client.add_milestone(1, {"name": "Release 2.0", "due_on": 1394596385})
    # @example Request
    #   {"name": "Release 2.0", "due_on": 1394596385}
    # @example Return
    #   If successful, this method returns the updated section using the same response format as get_section.
    # @see http://docs.gurock.com/testrail-api2/reference-milestones
    def add_milestone(project_id, opts = {})
      send_post("add_milestone/#{project_id.to_s}", opts)
    end

  ####update_milestone########
    # Add milestone to project id
    # @param [Int]  milestone_id
    # @param [Hash] opts
    # @option opts [bool] is_completed
    # @option opts [bool] is_started
    # @option opts [int] parent_id
    # @option opts [timestamp] start_on
    # @example Endpoint
    #   index.php?/api/v2/update_milestone/:milestone_id
    # @example Code
    #  client.update_milestone(1, {"is_completed": false, "is_started": false, "parent_id": 1, "start_on": 1394596385})
    # @example Request
    #   {"is_completed": false, "is_started": false, "parent_id": 1, "start_on": 1394596385}
    # @example Return
    #   If successful, this method returns the updated section using the same response format as get_section.
    # @see http://docs.gurock.com/testrail-api2/reference-milestones
    def update_milestone(milestone_id, opts = {})
      send_post("update_milestone/#{milestone_id.to_s}", opts)
    end

  ####delete_milestone########
    # Add milestone to project id WARNING: Destructive
    # @param [Int]  milestone_id
    # @example Endpoint
    #   index.php?/api/v2/delete_milestone/1
    # @example Code
    #   client.delete_milestone(1)
    # @see http://docs.gurock.com/testrail-api2/reference-milestones
    def delete_milestone(milestone_id, opts = {})
      send_post("delete_milestone/#{milestone_id.to_s}", opts)
    end

#### PLANS ###########+++++++++9

  ####get_plan################
    # Get plan by id
    # @param [Int]  plan_id
    # @example Endpoint
    #   GET index.php?/api/v2/get_plan/1
    # @example Code
    #   client.get_plan(1)
    # @example Return
    #   {
    #      "assignedto_id": null,
    #      "blocked_count": 2,
    #      "completed_on": null,
    #      "created_by": 1,
    #      "created_on": 1393845644,
    #      "custom_status1_count": 0,
    #      "custom_status2_count": 0,
    #      "custom_status3_count": 0,
    #      "custom_status4_count": 0,
    #      "custom_status5_count": 0,
    #      "custom_status6_count": 0,
    #      "custom_status7_count": 0,
    #      "description": null,
    #      "entries": [
    #      {
    #        "id": "3933d74b-4282-4c1f-be62-a641ab427063",
    #        "name": "File Formats",
    #        "runs": [
    #        {
    #          "assignedto_id": 6,
    #          "blocked_count": 0,
    #          "completed_on": null,
    #          "config": "Firefox, Ubuntu 12",
    #          "config_ids": [
    #            2,
    #            6
    #          ],
    #          "custom_status1_count": 0,
    #          "custom_status2_count": 0,
    #          "custom_status3_count": 0,
    #          "custom_status4_count": 0,
    #          "custom_status5_count": 0,
    #          "custom_status6_count": 0,
    #          "custom_status7_count": 0,
    #          "description": null,
    #          "entry_id": "3933d74b-4282-4c1f-be62-a641ab427063",
    #          "entry_index": 1,
    #          "failed_count": 2,
    #          "id": 81,
    #          "include_all": false,
    #          "is_completed": false,
    #          "milestone_id": 7,
    #          "name": "File Formats",
    #          "passed_count": 2,
    #          "plan_id": 80,
    #          "project_id": 1,
    #          "retest_count": 1,
    #          "suite_id": 4,
    #          "untested_count": 3,
    #          "url": "http://<server>/testrail/index.php?/runs/view/81"
    #        },
    #        {
    #          ..
    #        }
    #        ],
    #        "suite_id": 4
    #      }
    #      ],
    #      "failed_count": 2,
    #      "id": 80,
    #      "is_completed": false,
    #      "milestone_id": 7,
    #      "name": "System test",
    #      "passed_count": 5,
    #      "project_id": 1,
    #      "retest_count": 1,
    #      "untested_count": 6,
    #      "url": "http://<server>/testrail/index.php?/plans/view/80"
    #    }
    # @see http://docs.gurock.com/testrail-api2/reference-plans
    def get_plan(plan_id)
      send_get("get_plan/#{plan_id.to_s}")
    end

  ####get_plans###############
    # @param project_id [int]
    # @param [Hash] opts
    # @example Endpoint
    #   index.php?/api/v2/get_plans/:project_id
    # @see http://docs.gurock.com/testrail-api2/reference-plans
    # Get plans in project by project id
    # @param [Int]  project_id
    # @param [Hash] opts
    # @option opts [timestamp] created_after
    # @option opts [timestamp] created_before
    # @option opts [string] created_by
    # @option opts [bool] is_completed
    # @option opts [int] limit
    # @option opts [int] offset
    # @option opts [int(list)] milestone_id
    # @example Endpoint
    #   GET index.php?/api/v2/get_plans/1
    # @example Code
    #
    # @example Request
    #   {"is_completed":1, "milestone_id": 1 }
    # @example Return
    #   {
    #
    #   }
    # @see
    def get_plans(project_id, opts = {})
      options = param_stringify(opts)
      puts options
      send_get("get_plans/#{project_id.to_s}&#{options}")
    end

  ####add_plan################
    # Creates a new test plan.
    # @param [Int]  project_id
    # @param [Hash] opts
    # @option opts [string] name
    # @option opts [string] description
    # @option opts [int]    milestone_id
    # @option opts [array]  entries
    # @example Endpoint
    #   POST index.php?/api/v2/add_plan/1
    # @example Code
    #   client.add_plan(1, {"name": "foo", "description": "bar"})
    # @example Request (plain)
    #    {
    #      "name": "System test",
    #      "entries": [
    #        {
    #          "suite_id": 1,
    #          "name": "Custom run name",
    #          "assignedto_id": 1
    #        },
    #        {
    #          "suite_id": 1,
    #          "include_all": false,
    #          "case_ids": [1,2,3,5]
    #        }
    #      ]
    #    }
    # @example Request (With configurations. See gurock docs for more info)
    #   {
    #      "name": "System test",
    #      "entries": [
    #        {
    #          "suite_id": 1,
    #          "include_all": true,
    #          "config_ids": [1, 2, 4, 5, 6],
    #          "runs": [
    #            {
    #              "include_all": false,
    #              "case_ids": [1, 2, 3],
    #              "assignedto_id": 1,
    #              "config_ids": [2, 5]
    #            },
    #            {
    #              "include_all": false,
    #              "case_ids": [1, 2, 3, 5, 8],
    #              "assignedto_id": 2,
    #              "config_ids": [2, 6]
    #            }
    #           ..
    #          ]
    #        },
    #       ..
    #     ]
    #    }
    # @example Return
    #   If successful, this method returns the updated section using the same response format as get_section.
    # @see http://docs.gurock.com/testrail-api2/reference-plans
    def add_plan(project_id, opts = {})
      send_post("add_plan/#{project_id.to_s}", opts)
    end

  ####add_plan_entry##########
    # Add plan entries by plan id
    # @param [Int]  plan_id
    # @param [Hash] opts
    # @option opts [int]    suite_id
    # @option opts [string] name
    # @option opts [string] description
    # @option opts [int]    assignedto_id
    # @option opts [bool]   include_all
    # @option opts [array]  case_ids
    # @option opts [array]  config_ids
    # @option opts [array]  runs
    # @example Endpoint
    #   index.php?/api/v2/add_plan_entry/1
    # @example Code
    #   options =
    #    {
    #      "suite_id": 1,
    #      "assignedto_id": 1,           // Default assignee
    #      "include_all": true,          // Default selection
    #      "config_ids": [1, 2, 4, 5, 6],
    #      "runs": [
    #        {
    #          "include_all": false, // Override selection
    #          "case_ids": [1, 2, 3],
    #          "config_ids": [2, 5]
    #        },
    #        {
    #          "include_all": false, // Override selection
    #          "case_ids": [1, 2, 3, 5, 8],
    #          "assignedto_id": 2,   // Override assignee
    #          "config_ids": [2, 6]
    #        }
    #       ..
    #      ]
    #    }
    #   client.add_plan_entry(1, options)
    # @example Request
    #    {
    #      "suite_id": 1,
    #      "assignedto_id": 1,           // Default assignee
    #      "include_all": true,          // Default selection
    #      "config_ids": [1, 2, 4, 5, 6],
    #      "runs": [
    #        {
    #          "include_all": false, // Override selection
    #          "case_ids": [1, 2, 3],
    #          "config_ids": [2, 5]
    #        },
    #        {
    #          "include_all": false, // Override selection
    #          "case_ids": [1, 2, 3, 5, 8],
    #          "assignedto_id": 2,   // Override assignee
    #          "config_ids": [2, 6]
    #        }
    #       ..
    #      ]
    #    }
    # @example Return
    #   If successful, this method returns the new test plan entry including test runs using the same response format as the entries field of get_plan, but for a single entry instead of a list of entries.
    # @see http://docs.gurock.com/testrail-api2/reference-plans
    def add_plan_entry(plan_id, opts = {})
      send_post("add_plan_entry/#{plan_id.to_s}", opts)
    end

  ####update_plan#############
    # Updates an existing test plan (partial updates are supported, i.e. you can submit and update specific fields only).
    # @param [Int]  plan_id
    # @param [Hash] opts
    # @option opts [string] name
    # @option opts [string] description
    # @option opts [int]    milestone_id
    # @example Endpoint
    #   POST index.php?/api/v2/update_plan/1
    # @example Code
    #   client.update_plan(1, {"name": "foo", "description": "bar"})
    # @example Return
    #   If successful, this method returns the updated test plan using the same response format as get_plan.
    # @see http://docs.gurock.com/testrail-api2/reference-plans
    def update_plan(plan_id, opts = {})
      send_post("update_plan/#{plan_id.to_s}", opts)
    end

  ####update_plan_entry#######
    # Updates one or more existing test runs in a plan (partial updates are supported, i.e. you can submit and update specific fields only).
    # @param [int] plan_id
    # @param [int] entry_id
    # @param [Hash] opts
    # @option opts [string]  name
    # @option opts [string]  description
    # @option opts [int]     assignedto_id
    # @option opts [bool]    include_all
    # @option opts [array]   case_ids
    # @example Endpoint
    #   POST index.php?/api/v2/update_plan_entry/1/1
    # @example Code
    #   client.update_plan_entry(1, 1, {"name": "foo", "description": "bar"})
    # @see http://docs.gurock.com/testrail-api2/reference-plans
    def update_plan_entry(plan_id, entry_id, opts = {})
      send_post("update_plan_entry/#{plan_id.to_s}/#{entry_id.to_s}", opts)
    end

  ####close_plan##############
    # Closes an existing test plan and archives its test runs & results. WARNING: This can not be undone.
    # @param [int] plan_id
    # @example Endpoint
    #   POST index.php?/api/v2/close_plan/1
    # @example Code
    #   client.close_plan(1)
    # @example Response
    #   If successful, this method returns the closed test plan using the same response format as get_plan.
    # @see http://docs.gurock.com/testrail-api2/reference-plans
    def close_plan(plan_id, opts = {})
      send_post("close_plan/#{plan_id.to_s}", opts)
    end

  ####delete_plan#############
    # Deletes an existing test plan. WARNING: This can not be undone. Permenantly deletes ALL test runs and results in plan
    # @param [int] plan_id
    # @example Endpoint
    #   POST index.php?/api/v2/delete_plan/1
    # @example Code
    #   client.delete_plan(1)
    # @example Response
    #   If successful, this method returns the closed test plan using the same response format as get_plan.
    # @see http://docs.gurock.com/testrail-api2/reference-plans
    def delete_plan(plan_id, opts = {})
      send_post("delete_plan/#{plan_id.to_s}", opts)
    end

  ####delete_plan_by_entry####
    # Deletes one or more existing test runs from a plan. WARNING: Deletes test results
    # @param [int] plan_id
    # @param [int] entry_id
    # @example Endpoint
    #   POST index.php?/api/v2/delete_plan_entry/1/1
    # @example Code
    #   client.delete_plan_entry(1, 1)
    # @see http://docs.gurock.com/testrail-api2/reference-plans
    def delete_plan_entry(plan_id, entry_id, opts = {})
      send_post("delete_plan_entry/#{plan_id.to_s}/#{entry_id.to_s}", opts)
    end

#### PROJECTS ############+++++5

  ####get_project#############
    # Get project by project id
    # @param project_id [int]
    # @example Endpoint Example
    #   GET index.php?/api/v2/get_project/1
    # @example Code
    #   client.get_project(1)
    # @see http://docs.gurock.com/testrail-api2/reference-projects
    def get_project(project_id, opts = {})
      options = param_stringify(opts)
      send_get("get_project/#{project_id.to_s}&#{options}")
    end

  ####get_projects############
    # Get all projects
    # @param [Hash] opts
    # @option opts [bool] :is_completed 1 == completed projects, 2 == active projects
    # @example Endpoint Example
    #   GET index.php?/api/v2/get_projects
    # @example Code Example [get all projects]
    #   client.get_projects
    # @example Code Example [get active projects]
    #   client.get_projects({'is_completed':0})
    # @example Code Example [get completed projects]
    #   client.get_projects({'is_completed':1})
    # @example Response Example
    #   [
    #    { "id": 1, "name": "Project1", ... },
    #    { "id": 2, "name": "Project2", ... },
    #    ..
    #   ]
    # @see http://docs.gurock.com/testrail-api2/reference-projects
    def get_projects(opts = {})
      options = param_stringify(opts)
      send_get("get_projects&#{options}")
    end

  ####add_project#############
    # Creates a new project (admin status required)
    # @param [Hash] opts
    # @option [string] name
    # @option [string] announcement
    # @option [bool]   show_announcement
    # @option [int]    suite_mode
    # @example Endpoint Example
    #   POST index.php?/api/v2/add_project
    # @example Code
    #   client.add_project({"name": "foo", "announcement": "bar", "show_announcement": true, "suite_mode": 1})
    # @example Request
    #   {"name": "foo", "announcement": "bar", "show_announcement": true, "suite_mode": 1}
    # @example Response
    #   see get_project
    # @see http://docs.gurock.com/testrail-api2/reference-projects
    def add_project(opts = {})
      send_post("add_project", opts)
    end

  ####update_project##########
    # Updates an existing project (admin status required; partial updates are supported, i.e. you can submit and update specific fields only).
    # @param project_id [int] ID of project to update
    # @param [Hash] opts
    # @option [string] name
    # @option [string] announcement
    # @option [bool]   show_announcement
    # @option [int]    suite_mode
    # @option [bool]    is_completed
    # @example Endpoint Example
    #   POST index.php?/api/v2/update_project/:project_id
    # @example Code
    #   client.update_project(1, {"name": "foo", "announcement": "bar", "show_announcement": true, "suite_mode": 1, "is_completed": true})
    # @example Request
    #   {"name": "foo", "announcement": "bar", "show_announcement": true, "suite_mode": 1, "is_completed": true}
    # @example Response
    #   see get_project
    # @see http://docs.gurock.com/testrail-api2/reference-projects
    def update_project(project_id, opts = {})
      send_post("update_project/#{project_id.to_s}", opts)
    end

  ####delete_project##########
    # Delete an existing project
    # @param project_id [int] The project you want to delete
    # @example Endpoint Example
    #   POST index.php?/api/v2/delete_project/1
    # @example Code
    #   client.delete_project(1)
    # @see http://docs.gurock.com/testrail-api2/reference-projects
    def delete_project(project_id, opts = {})
      send_post("delete_project/#{project_id.to_s}", opts)
    end

#### RESULTS ###########+++++++7

  ####get_results#############
    # Returns a list of test results for a test
    # @param [int]  test_id
    # @param [Hash] opts
    # @option opts [int] limit
    # @option opts [int] offset
    # @option opts [int(list)] status_id
    # @example Endpoint Example
    #   GET index.php?/api/v2/get_results/1&status_id=4,5&limit=10
    # @example Code
    #   client.get_results(1, {"status_id": 4, "limit": 10})
    # @example Request
    #   {"status_id": 4, "limit": 10}
    # @example Return
    # @see http://docs.gurock.com/testrail-api2/reference-results
    def get_results(test_id, opts = {})
      options = param_stringify(opts)
      send_get("get_results/#{test_id.to_s}&#{options}")
    end

  ####get_results_for_case####
    # Returns a list of test results for a test run and case combination
    # @param [int] run_id
    # @param [int] case_id
    # @param [Hash] opts
    # @option opts [int] limit
    # @option opts [int] offset
    # @option opts [int(list)] status_id
    # @example Endpoint Example
    #   GET index.php?/api/v2/get_results_for_case/1/2
    # @example Code
    #   client.get_results_for_case(1, 2, {"status_id": 4, "limit": 10})
    # @example Request
    #   {"status_id": 4, "limit": 10}
    # @example Return
    #   see get_results
    # @see http://docs.gurock.com/testrail-api2/reference-results
    def get_results_for_case(run_id, case_id, opts = {})
      options = param_stringify(opts)
      send_get("get_results_for_case/#{run_id.to_s}/#{case_id.to_s}&#{options}")
    end

  ####get_results_for_run#####
    # Returns a list of test results for a test run
    # @param [int] run_id
    # @param [Hash] opts
    # @option opts [timestamp]  created_after
    # @option opts [timestamp]  created_before
    # @option opts [int(list)]  created_by
    # @option opts [int]        limit
    # @option opts [int]        offset
    # @option opts [int(list)]  status_id
    # @example Endpoint Example
    #   GET index.php?/api/v2/get_results_for_run/1
    # @example Code
    #   client.get_results_for_run(1, {"created_after": 12345, "created_before": 12345, "created_by": 1, "limit": 5, "status_id": 1})
    # @example Request
    #   {"created_after": 12345, "created_before": 12345, "created_by": 1, "limit": 5, "status_id": 1}
    # @example Return
    #   see get_results
    # @see http://docs.gurock.com/testrail-api2/reference-results
    def get_results_for_run(run_id, opts = {})
      options = param_stringify(opts)
      send_get("get_results_for_run/#{run_id.to_s}&#{options}")
    end

  ####add_result##############
    # Adds a new test result, comment or assigns a test. It's recommended to use add_results instead if you plan to add results for multiple tests.
    # @param [int] test_id
    # @param [Hash] opts
    # @option opts [int]      status_id
    # @option opts [string]   comment
    # @option opts [string]   version
    # @option opts [timespan] elapsed
    # @option opts [string]   defects
    # @option opts [int]      assignedto_id
    # @option opts [bool]     custom_checkbox
    # @option opts [string]   custom_dropdown
    # @option opts [int]      custom_integer
    # @option opts [int]      custom_milestone
    # @option opts [int]      custom_multi-select
    # @option opts [array]    custom_step_results
    # @option opts [array]    custom_string
    # @option opts [string]   custom_text
    # @option opts [string]   custom_url
    # @option opts [string]   custom_user
    # @example Endpoint Example
    #   POST index.php?/api/v2/add_result/1
    # @example Code
    #   options = json
    #   client.add_result(1, options)
    # @example Request
    #   {
    #     "status_id": 5,
    #     "comment": "This test failed",
    #     "elapsed": "15s",
    #     "defects": "TR-7",
    #     "version": "1.0 RC1 build 3724",
    #     "custom_step_results": [
    #       {
    #         "content": "Step 1",
    #         "expected": "Expected Result 1",
    #         "actual": "Actual Result 1",
    #         "status_id": 1
    #       },
    #       {
    #         "content": "Step 2",
    #         "expected": "Expected Result 2",
    #         "actual": "Actual Result 2",
    #         "status_id": 2
    #       },
    #     ]
    #   }
    # @example Return
    #   see get_results
    # @see http://docs.gurock.com/testrail-api2/reference-results
    def add_result(test_id, opts = {})
      send_post("add_result/#{test_id.to_s}", opts)
    end

  ####add_result_for_case#####
    # Adds a new test result, comment or assigns a test (for a test run and case combination)
    # @param [int]  run_id
    # @param [int]  case_id
    # @param [Hash] opts  see add_result for options
    # @example Endpoint Example
    #   POST index.php?/api/v2/add_result_for_case/1/1
    # @example Code
    #   options = json
    #   client.add_result(1, 1, options)
    # @example Request
    #   {
    #     "status_id": 5,
    #     "comment": "This test failed",
    #     "elapsed": "15s",
    #     "defects": "TR-7",
    #     "version": "1.0 RC1 build 3724",
    #     "custom_step_results": [
    #       {
    #         "content": "Step 1",
    #         "expected": "Expected Result 1",
    #         "actual": "Actual Result 1",
    #         "status_id": 1
    #       },
    #       {
    #         "content": "Step 2",
    #         "expected": "Expected Result 2",
    #         "actual": "Actual Result 2",
    #         "status_id": 2
    #       },
    #     ]
    #   }
    # @example Return
    #   see get_results
    # @see http://docs.gurock.com/testrail-api2/reference-results
    def add_result_for_case(run_id, case_id, opts = {})
      send_post("add_result_for_case/#{run_id.to_s}/#{case_id.to_s}", opts)
    end

  ####add_results#############
    # Adds one or more new test results, comments or assigns one or more tests
    # @param run_id [int]
    # @param [Hash] opts
    # @option [varies] see add_result
    # @example Endpoint Example
    #   index.php?/api/v2/add_results/1
    # @example Code
    #   opts = json
    #   client.add_results(1, opts)
    # @example Request
    #    {
    #      "results": [
    #        {
    #          "test_id": 101,
    #          "status_id": 5,
    #          "comment": "This test failed",
    #          "defects": "TR-7"

    #        },
    #        {
    #          "test_id": 102,
    #          "status_id": 1,
    #          "comment": "This test passed",
    #          "elapsed": "5m",
    #          "version": "1.0 RC1"
    #        },

    #        ..

    #        {
    #          "test_id": 101,
    #          "assignedto_id": 5,
    #          "comment": "Assigned this test to Joe"
    #        }

    #        ..
    #      ]
    #    }
    # @example Return
    #   see get_results
    # @see http://docs.gurock.com/testrail-api2/reference-results
    def add_results(run_id, opts = {})
      send_post("add_results/#{run_id.to_s}", opts)
    end

  ####add_results_for_cases###
    # Adds one or more new test results, comments or assigns one or more tests (using the case IDs)
    # @param [int]  run_id
    # @param [Hash] opts
    # @option [varies] see add_result
    # @example Endpoint Example
    #   index.php?/api/v2/add_results_for_cases/1
    # @example Code
    #   opts =
    #   client.add_results_for_cases(1, opts)
    # @example Request
    #   Expects case_id instead of test_id
    #    {
    #      "results": [
    #        {
    #          "case_id": 1,
    #          "status_id": 5,
    #          "comment": "This test failed",
    #          "defects": "TR-7"
    #        },
    #        {
    #          "case_id": 2,
    #          "status_id": 1,
    #          "comment": "This test passed",
    #          "elapsed": "5m",
    #          "version": "1.0 RC1"
    #        },
    #        {
    #          "case_id": 1,
    #          "assignedto_id": 5,
    #          "comment": "Assigned this test to Joe"
    #        }
    #      ]
    #    }
    # @example Return
    #   see get_results
    # @see http://docs.gurock.com/testrail-api2/reference-results
    def add_results_for_cases(run_id, opts = {})
      send_post("add_results_for_cases/#{run_id.to_s}", opts)
    end

#### RUNS ###############++++++6

  ####get_run#################
    # Get run by run id
    # @param [int]  run_id
    # @example Endpoint Example
    #   GET index.php?/api/v2/get_run/1
    # @example Code
    #   client.get_run(1)
    # @example Return
    #    {
    #      "assignedto_id": 6,
    #      "blocked_count": 0,
    #      "completed_on": null,
    #      "config": "Firefox, Ubuntu 12",
    #      "config_ids": [
    #        2,
    #        6
    #      ],
    #      "created_by": 1,
    #      "created_on": 1393845644,
    #      "custom_status1_count": 0,
    #      "custom_status2_count": 0,
    #      "custom_status3_count": 0,
    #      "custom_status4_count": 0,
    #      "custom_status5_count": 0,
    #      "custom_status6_count": 0,
    #      "custom_status7_count": 0,
    #      "description": null,
    #      "failed_count": 2,
    #      "id": 81,
    #      "include_all": false,
    #      "is_completed": false,
    #      "milestone_id": 7,
    #      "name": "File Formats",
    #      "passed_count": 2,
    #      "plan_id": 80,
    #      "project_id": 1,
    #      "retest_count": 1,
    #      "suite_id": 4,
    #      "untested_count": 3,
    #      "url": "http://<server>/testrail/index.php?/runs/view/81"
    #    }
    # @see http://docs.gurock.com/testrail-api2/reference-runs
    def get_run(run_id)
      send_get("get_run/#{run_id.to_s}")
    end

  ####get_runs################
    # Get runs by project id
    # @param [int]  run_id
    # @param [hash] opts
    # @option [timestamp] created_after
    # @option [timestamp] created_before
    # @option [int(list)] created_by
    # @option [bool]      is_completed
    # @option [int]       limit
    # @option [int]       offset
    # @option [int(list)] milestone_id
    # @option [int(list)] suite_id
    # @example Endpoint Example
    #   index.php?/api/v2/get_runs/1
    # @example Code
    #   opts = json
    #   client.get_runs(1, opts)
    # @example Return
    #    {
    #      "assignedto_id": 6,
    #      "blocked_count": 0,
    #      "completed_on": null,
    #      "config": "Firefox, Ubuntu 12",
    #      "config_ids": [
    #        2,
    #        6
    #      ],
    #      "created_by": 1,
    #      "created_on": 1393845644,
    #      "custom_status1_count": 0,
    #      "custom_status2_count": 0,
    #      "custom_status3_count": 0,
    #      "custom_status4_count": 0,
    #      "custom_status5_count": 0,
    #      "custom_status6_count": 0,
    #      "custom_status7_count": 0,
    #      "description": null,
    #      "failed_count": 2,
    #      "id": 81,
    #      "include_all": false,
    #      "is_completed": false,
    #      "milestone_id": 7,
    #      "name": "File Formats",
    #      "passed_count": 2,
    #      "plan_id": 80,
    #      "project_id": 1,
    #      "retest_count": 1,
    #      "suite_id": 4,
    #      "untested_count": 3,
    #      "url": "http://<server>/testrail/index.php?/runs/view/81"
    #    }
    # @see http://docs.gurock.com/testrail-api2/reference-runs
    def get_runs(project_id, opts = {})
      options = param_stringify(opts)
      send_get("get_runs/#{project_id.to_s}&#{options}")
    end

  ####add_run#################
    # Add run by suite id
    # @param [int]  project_id
    # @param [Hash] opts
    # @option [int]     suite_id
    # @option [string]  name
    # @option [string]  description
    # @option [int]     milestone_id
    # @option [int]     assignedto_id
    # @option [bool]    include_all
    # @option [array]   case_ids
    # @example Endpoint Example
    #   POST index.php?/api/v2/add_run/1
    # @example Code
    #   opts = json
    #   client.add_run(1, opts)
    # @example Request
    #    {
    #      "suite_id": 1,
    #      "name": "This is a new test run",
    #      "assignedto_id": 5,
    #      "include_all": false,
    #      "case_ids": [1, 2, 3, 4, 7, 8]
    #    }
    # @example Return
    #   see get_run
    # @see http://docs.gurock.com/testrail-api2/reference-runs
    def add_run(project_id, opts = {})
      send_post("add_run/#{project_id.to_s}", opts)
    end

  ####update_run##############
    # Updates existing test run
    # @param run_id [int]
    # @param [Hash] opts
    # @option [string]  name
    # @option [string]  description
    # @option [int]     milestone_id
    # @option [bool]    include_all
    # @option [array]   case_ids
    # @example Endpoint Example
    #   POST index.php?/api/v2/update_run/1
    # @example Code
    #   opts = json
    #   client.update_run(1, opts)
    # @example Request
    #    {
    #      "name": "This is a test run",
    #      "include_all": false
    #    }
    # @see http://docs.gurock.com/testrail-api2/reference-runs
    def update_run(run_id, opts = {})
      send_post("update_run/#{run_id.to_s}", opts)
    end

  ####close_run###############
    # Closes an existing test run and archives its tests & results
    # @param [int] run_id
    # @example Endpoint Example
    #   POST index.php?/api/v2/close_run/1
    # @example Code
    #   client.close_run(1)
    # @see http://docs.gurock.com/testrail-api2/reference-runs
    def close_run(run_id, opts = {})
      send_post("close_run/#{run_id.to_s}", opts)
    end

  ####delete_run##############
    # Deletes an existing test run.
    # @param run_id [int]
    # @example Endpoint Example
    #   POST index.php?/api/v2/delete_run/1
    # @example Code
    #   client.delete_run(1)
    # @see http://docs.gurock.com/testrail-api2/reference-runs
    def delete_run(run_id, opts = {})
      send_post("delete_run/#{run_id.to_s}", opts)
    end

#### TESTS ##################++2

  ####get_test################
    # Returns an existing test
    # @param test_id [int]
    # @example Endpoint Example
    #   GET index.php?/api/v2/get_test/1
    # @example Code
    #   client.get_test(1)
    # @example Return
    #    {
    #      "assignedto_id": 1,
    #      "case_id": 1,
    #      "custom_expected": "..",
    #      "custom_preconds": "..",
    #      "custom_steps_separated": [
    #        {
    #          "content": "Step 1",
    #          "expected": "Expected Result 1"
    #        },
    #        {
    #          "content": "Step 2",
    #          "expected": "Expected Result 2"
    #        }
    #      ],
    #      "estimate": "1m 5s",
    #      "estimate_forecast": null,
    #      "id": 100,
    #      "priority_id": 2,
    #      "run_id": 1,
    #      "status_id": 5,
    #      "title": "Verify line spacing on multi-page document",
    #      "type_id": 4
    #    }
    # @see http://docs.gurock.com/testrail-api2/reference-tests
    def get_test(test_id)
      send_get("get_test/#{test_id.to_s}")
    end

  ####get_tests###############
    # Returns an existing test
    # @param run_id [int]
    # @param opts [hash]
    # @option [int(list)] status_id
    # @example Endpoint Example
    #   GET index.php?/api/v2/get_tests/1
    # @example Code
    #   client.get_tests(1, {"status_id": 1})
    # @example Return
    #    [
    #      {
    #        "id": 1,
    #        "title": "Test conditional formatting with basic value range",
    #      },
    #      {
    #        "id": 2,
    #        "title": "Verify line spacing on multi-page document",
    #      },
    #    ]
    # @see http://docs.gurock.com/testrail-api2/reference-tests
    def get_tests(run_id, opts = {})
      options = param_stringify(opts)
      send_get("get_tests/#{run_id.to_s}&#{options}")
    end


end #close module
